package me.flyray.bsin.facade.service;

import java.util.Map;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * 智能决策模型
 */

@Path("decisionModel")
public interface DecisionModelService {

    /**
     * 决策模型添加
     */

    /**
     * 决策模型部署
     * 将决策组件加载到规则库中
     */
    @POST
    @Path("deploy")
    @Produces("application/json")
    public Map<String, Object> deploy(Map<String, Object> requestMap) throws ClassNotFoundException;

    /**
     * 决策模型停用
     */


}

package me.flyray.bsin.server;

import org.flowable.engine.RuntimeService;
import org.flowable.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author ：bolei
 * @date ：Created in 2020/9/15 9:31
 * @description：流程实例管理
 * @modified By：
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class BsinWorkflowEngineProcessInstanceTests {

    @Autowired
    private RuntimeService runtimeService;

    /**
     * 流程实例查询
     */
    @Test
    public void processInstanceQuery(){
       ProcessInstance processInstance =  runtimeService.createProcessInstanceQuery().processInstanceId("4cc75b57-fa5c-11ea-b8df-982cbc048a9e").singleResult();
        System.out.println(processInstance.getProcessDefinitionId());
    }

    /**
     * 流程实例启动
     */
    @Test
    public void processInstanceStartByKey(){
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("leaveProcessDemo");
        System.out.println("流程实例ID:" + processInstance.getId());// 流程实例ID 101
        System.out.println("流程定义ID:" + processInstance.getProcessDefinitionId());
    }

    /**
     * 流程实例撤销
     * String processInstanceId, String deleteReason
     */
    @Test
    public void deleteProcessInstance(){
        String processInstanceId = "4cc75b57-fa5c-11ea-b8df-982cbc048a9e";
        String deleteReason = "撤销申请";
        runtimeService.deleteProcessInstance(processInstanceId,deleteReason);
        System.out.println( processInstanceId + "流程实例撤销成功");
    }


}

package me.flyray.bsin.server.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

import me.flyray.bsin.server.domain.ActDeModel;

@Repository
@Mapper
public interface ActDeModelMapper {
    List<ActDeModel> getProcessModelByType(@Param("tenantId") String tenantId,@Param("modelTypeId")  String modelTypeId);

    List<ActDeModel> getFormModel(@Param("tenantId") String tenantId, @Param("modelType") String modelType,@Param("modelKey") String modelKey);

    int setActDeModelTenantId (@Param("id") String id ,@Param("tenantId") String tenantId);

    ActDeModel getActDeModelKey(String model_key);
}

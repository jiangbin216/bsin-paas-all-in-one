package me.flyray.bsin.server.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import me.flyray.bsin.facade.service.BsinAdminProcessInstanceTaskService;
import me.flyray.bsin.server.domain.Task;
import me.flyray.bsin.server.mapper.TaskMapper;
import me.flyray.bsin.utils.RespBodyHandler;
import org.flowable.engine.TaskService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 流程任务
 */

public class ProcessInstanceTaskServiceImpl implements BsinAdminProcessInstanceTaskService {

    @Autowired
    private TaskMapper taskMapper;
    @Autowired
    private TaskService taskService;
    /**
     * 查询流程实例任务
     * @param requestMap
     * @return
     */
    @Override
    public Map<String, Object> getProcessInstanceTasks(Map<String, Object> requestMap) {
        Map<String, Object> pagination = (Map<String, Object>) requestMap.get("pagination");
        String processInstanceId = (String)requestMap.get("processInstanceId");
        String processDefinitionName = (String)requestMap.get("processDefinitionName");
        String owner = (String)requestMap.get("owner");
        String assignee = (String)requestMap.get("assignee");
        String name = (String)requestMap.get("name");
        Map<String, Object> map=new HashMap<>();
        map.put("processInstanceId",processInstanceId);
        map.put("processDefinitionName",processDefinitionName);
        map.put("owner",owner);
        map.put("assignee",assignee);
        map.put("name",name);
        List<Task> pageList = taskMapper.selectTasks(map);
        PageInfo<Task> pageInfo = new PageInfo<>(pageList);
        return RespBodyHandler.setRespPageInfoBodyDto(pageInfo);
    }

    /**
     * 查询历史实例任务
     * @param requestMap
     * @return
     */
    @Override
    public Map<String, Object> getHistoricInstanceTasks(Map<String, Object> requestMap) {
        Map<String, Object> pagination = (Map<String, Object>) requestMap.get("pagination");
        String processInstanceId = (String)requestMap.get("processInstanceId");
        String processDefinitionName = (String)requestMap.get("processDefinitionName");
        String owner = (String)requestMap.get("owner");
        String assignee = (String)requestMap.get("assignee");
        String name = (String)requestMap.get("name");
        Map<String, Object> map=new HashMap<>();
        map.put("processInstanceId",processInstanceId);
        map.put("processDefinitionName",processDefinitionName);
        map.put("owner",owner);
        map.put("assignee",assignee);
        map.put("name",name);
        PageHelper.startPage((Integer) pagination.get("pageNum"), (Integer) pagination.get("pageSize"));
        List<Task> pageList = taskMapper.selectHistoricTasks(map);
        PageInfo<Task> pageInfo = new PageInfo<>(pageList);
        return RespBodyHandler.setRespPageInfoBodyDto(pageInfo);
    }

   //  __________________________________________________________________________________________________

    /**
     * 查询所有未暂停的任务
     */
    @Override
    public Map<String, Object> getAllTask(Map<String, Object> requestMap)  {
        Map<String, Object> pagination = (Map<String, Object>) requestMap.get("pagination");
        String tenantId =(String) requestMap.get("tenantId");
        List<org.flowable.task.api.Task> taskList = taskService.createTaskQuery()
                .active()
                .taskTenantId(tenantId)
                .orderByTaskCreateTime()
                .desc()
                .list();
        List<org.flowable.task.api.Task> partList = getPartList(taskList, (Integer) pagination.get("pageNum"), (Integer) pagination.get("pageSize"));
        PageInfo<org.flowable.task.api.Task> pageInfo = new PageInfo<>(partList);
        pageInfo.setTotal(taskList.size());
        return RespBodyHandler.setRespPageInfoBodyDto(pageInfo);
    }

    public static <T> List<T> getPartList(List<T> inLs,int page,int limit){
        page = (page <= 0) ? 1 : page;
        limit= (limit <= 0) ? 10 : limit;
        int total = inLs.size();
        return inLs.subList(Math.min((page - 1) * limit, total), Math.min(page * limit, total));
    }

    /**
     * 根据用户名获取待办任务
     */
    @Override
    public Map<String, Object> getTasksByUser(Map<String, Object> requestMap) {
        Map<String, Object> pagination = (Map<String, Object>) requestMap.get("pagination");
        String tenantId =(String) requestMap.get("tenantId");
        String owner =(String) requestMap.get("owner");
        String assignee =(String) requestMap.get("userId");
        PageHelper.startPage((Integer) pagination.get("pageNum"), (Integer) pagination.get("pageSize"));
        List<Task> pageList = taskMapper.selectTasksByUser(tenantId,assignee,owner);
        PageInfo<Task> pageInfo = new PageInfo<>(pageList);
        return RespBodyHandler.setRespPageInfoBodyDto(pageInfo);
    }

    /**
     * 根据用户名获取待办任务
     */
    @Override
    public Map<String, Object> getTasksByCustomerNo(Map<String, Object> requestMap) {
        Map<String, Object> pagination = (Map<String, Object>) requestMap.get("pagination");
        String tenantId =(String) requestMap.get("tenantId");
        String owner =(String) requestMap.get("owner");
        String assignee =(String) requestMap.get("customerNo");
        PageHelper.startPage((Integer) pagination.get("pageNum"), (Integer) pagination.get("pageSize"));
        List<Task> pageList = taskMapper.selectTasksByUser(tenantId,assignee,owner);
        PageInfo<Task> pageInfo = new PageInfo<>(pageList);
        return RespBodyHandler.setRespPageInfoBodyDto(pageInfo);
    }

    /**
     * 根据用户名获取候选任务
     */
    @Override
    public Map<String, Object> getCandidateTasksByUser(Map<String, Object> requestMap) {
        String tenantId =(String) requestMap.get("tenantId");
        String username =(String) requestMap.get("username");
        String processInstanceId =(String) requestMap.get("processInstanceId");
        List<org.flowable.task.api.Task> taskList = taskService.createTaskQuery()
                //设置taskCandidateUser候选人条件来查询任务，这里的参数值就是候选人流程变量绑定的值
                //员工1和员工2都是可以查询到数据的
//                .taskCandidateUser(username)
                .taskTenantId(tenantId)
                .list();
        return RespBodyHandler.setRespBodyListDto(taskList);
    }

    /**
     * 领取候选任务
     */
    @Override
    public Map<String, Object> claimCandidateTask(Map<String, Object> requestMap) {
        String userId =(String) requestMap.get("userId");
        String taskId =(String) requestMap.get("taskId");
        taskService.claim(taskId, userId);
        return RespBodyHandler.RespBodyDto();
    }

    /**
     * 任务归还（如果任务拾取之后不想操作或者误拾取任务也可以进行归还任务）
     */
    @Override
    public Map<String, Object> unClaimCandidateTask(Map<String, Object> requestMap) {
        String taskId =(String) requestMap.get("taskId");
        taskService.unclaim(taskId);
        return RespBodyHandler.RespBodyDto();
    }

    /**
     * 任务转办
     */
    @Override
    public Map<String, Object> transfer(Map<String, Object> requestMap) throws ClassNotFoundException {
        String taskId =(String) requestMap.get("taskId");
        String assignee =(String) requestMap.get("assignee");
        taskService.setAssignee(taskId, assignee);
        return RespBodyHandler.RespBodyDto();
    }

    /**
     * 任务委派
     */
    @Override
    public Map<String, Object> delegateTask(Map<String, Object> requestMap) throws ClassNotFoundException {
        String taskId =(String) requestMap.get("taskId");
        String assignee =(String) requestMap.get("assignee");
        taskService.delegateTask(taskId,assignee);
        return RespBodyHandler.RespBodyDto();
    }

    /**
     * 任务解决
     */
    @Override
    public Map<String, Object> resolve(Map<String, Object> requestMap) throws ClassNotFoundException {
        String taskId =(String) requestMap.get("taskId");
        Map<String, Object> variables =(Map<String, Object>) requestMap.get("variables");
        taskService.resolveTask(taskId, variables);
        return RespBodyHandler.RespBodyDto();
    }

    /**
     * 任务完成
     */
    @Override
    public Map<String, Object> complete(Map<String, Object> requestMap) throws ClassNotFoundException {
        String taskId =(String) requestMap.get("taskId");
        // 表单参数（id，value）
        Map<String, Object> variables =(Map<String, Object>) requestMap.get("variables");
        taskService.complete(taskId,variables);
        return RespBodyHandler.RespBodyDto();
    }

}

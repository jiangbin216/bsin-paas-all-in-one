package me.flyray.bsin.facade.service;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

@Path("model")
public interface BsinAdminModelService {
    /**
     * 保存对应model的属性信息
     * @param requestMap
     * @return
     */
    @POST
    @Path("saveModel")
    @Produces("application/json")
    Map<String, Object> saveModel(Map<String, Object> requestMap);

    /**
     * 保存对应model的xml信息
     * @param requestMap
     * @return
     */
    @POST
    @Path("saveModelXML")
    @Produces("application/json")
    Map<String, Object> saveModelXML(Map<String, Object> requestMap);


    /**
     * 修改模型
     * @param requestMap
     * @return
     */
    @POST
    @Path("updateModel")
    @Produces("application/json")
    Map<String, Object> updateModel(Map<String, Object> requestMap);

    /**
     * 删除模型
     * @param requestMap
     * @return
     */
    @POST
    @Path("deleteModel")
    @Produces("application/json")
    Map<String, Object> deleteModel(Map<String, Object> requestMap);

    /**
     * 分页查询所有模型
     * @param requestMap
     * @return
     */
    @POST
    @Path("getPageList")
    @Produces("application/json")
    Map<String, Object> getPageListModel(Map<String, Object> requestMap);

    /**
     * 部署/发布模型
     * @param requestMap
     * @return
     */
    @POST
    @Path("deployModel")
    @Produces("application/json")
    Map<String, Object> deployModel(Map<String, Object> requestMap);

    /**
     * 模型预览
     * @param requestMap
     * @return
     */
    @POST
    @Path("getModelById")
    @Produces("application/json")
    Map<String, Object> getModelById(Map<String, Object> requestMap);

    /**
     * 历史版本
     * @param requestMap
     * @return
     */
    @POST
    @Path("getHistoryVersions")
    @Produces("application/json")
    Map<String, Object> getHistoryVersions(Map<String, Object> requestMap);

    /**
     * 发布版本
     * @param requestMap
     * @return
     */
    @POST
    @Path("getModelDefinitionVersions")
    @Produces("application/json")
    Map<String, Object> getModelDefinitionVersions(Map<String, Object> requestMap);

    /**
     * 保存表单
     */
    @POST
    @Path("saveForm")
    @Produces("application/json")
    public Map<String, Object>saveForm(Map<String, Object> requestMap) throws ClassNotFoundException;

    /**
     * 保存表单信息
     */
    @POST
    @Path("saveFromModel")
    @Produces("application/json")
    public Map<String, Object>saveFormModel(Map<String, Object> requestMap) throws ClassNotFoundException;

    /**
     * 分页查询所有表单模型
     */
    @POST
    @Path("getPageListFromModel")
    @Produces("application/json")
    public Map<String, Object>getPageListFromModel(Map<String, Object> requestMap) throws ClassNotFoundException;

    /**
     * 获取表单信息
     */
    @POST
    @Path("getForm")
    @Produces("application/json")
    public Map<String, Object>getFormInfo(Map<String, Object> requestMap) throws ClassNotFoundException;

    /**
     * 部署表单
     */
    @POST
    @Path("deployForm")
    @Produces("application/json")
    public Map<String, Object>deployForm(Map<String, Object> requestMap) throws ClassNotFoundException;

}

package me.flyray.bsin.gateway.exception;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import me.flyray.bsin.exception.BusinessException;
import me.flyray.bsin.gateway.common.ApiResult;

@RestControllerAdvice
public class GlobalExceptionHandler {


    @ExceptionHandler(value = BusinessException.class)
    @ResponseBody
    public ApiResult handleBusinessException(BusinessException e) {
        e.printStackTrace();
        return ApiResult.fail(e.getCode(), e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ApiResult handleBusinessException(Exception e) {
        e.printStackTrace();
        return ApiResult.fail(e.getMessage());
    }


}

package me.flyray.bsin.facade.service;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

@Path("formRepositoryService")
public interface BsinFormRepositoryService {

    /**
     * 获取开始表单数据
     */
    @POST
    @Path("getStartFormData")
    @Produces("application/json")
    public Map<String, Object>getStartFormData(Map<String, Object> requestMap);

    /**
     * 获取任务表单数据
     */
    @POST
    @Path("getTaskFormDataBuiltIn")
    @Produces("application/json")
    public Map<String, Object>getTaskFormData(Map<String, Object> requestMap);

    /**
     * 提交开始表单参数
     */
    @POST
    @Path("submitStartFormData")
    @Produces("application/json")
    public Map<String, Object>submitStartFormData(Map<String, Object> requestMap);

    /**
     * 保存任务表单数据(内置)
     */
    @POST
    @Path("saveFormData")
    @Produces("application/json")
    public Map<String, Object>saveFormData(Map<String, Object> requestMap);

    /**
     * 完成任务并提交表单数据
     */
    @POST
    @Path("saveFormData")
    @Produces("application/json")
    public Map<String, Object>submitTaskFormData(Map<String, Object> requestMap) throws ClassNotFoundException;

    /**
     * 部署表单
     */
    @POST
    @Path("deployForm")
    @Produces("application/json")
    public Map<String, Object>deployForm(Map<String, Object> requestMap) throws ClassNotFoundException;

    /**
     * 获取开始表单数据
     */
    @POST
    @Path("getProcessDefinitionStartForm")
    @Produces("application/json")
    public Map<String, Object>getProcessDefinitionStartForm(Map<String, Object> requestMap) throws ClassNotFoundException;
}
